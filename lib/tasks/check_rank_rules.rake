


namespace :check_ranks do
  desc "Cheking the ranks"
  task check_ranks: :environment do
  	  
    @users = User.where('current_sign_in_at >= ?', 1.hour.ago)
  @users.each do |user|
  if user.points < 50
    user.update_attributes(level: 0)    
  elsif user.points >= 50 && user.points <= 150
    user.update_attributes(level: 1)  
  elsif user.points > 150 && user.points <= 300
    user.update_attributes(level: 2)      
  elsif user.points > 300 && user.points <= 800
    user.update_attributes(level: 3)  
  elsif user.points > 800 && user.points <= 2000
    user.update_attributes(level: 4)    
  elsif user.points > 2000 && user.points <= 4500
    user.update_attributes(level: 5)  
  elsif user.points > 4500 && user.points <= 7000
    user.update_attributes(level: 6)                 
  elsif user.points > 7000 && user.points <= 10000
    user.update_attributes(level: 7)  
  elsif user.points > 10000 && user.points <= 20000
    user.update_attributes(level: 8)     
  elsif user.points > 20000 && user.points <= 50000
    user.update_attributes(level: 9)  
  elsif user.points > 50000 && user.points <= 100000
    user.update_attributes(level: 10)           

  end

end


    
  end

end
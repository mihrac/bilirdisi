class CarrierWave::Uploader::Base
  add_config :delete_original_file
end

CarrierWave.configure do |config|
  config.delete_original_file = true
end
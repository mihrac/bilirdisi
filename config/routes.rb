Rails.application.routes.draw do
  
  

  

  resources :blogs do
    member do
        put :upvote
        put :downvote
        put :approve
        put :feature
        end 
    resources :comments, except: [:index, :new, :show] do
      member do
        put :select
        get :reply
        put :upvote        
        put :downvote
      end
    end

  end

  

  devise_for :users
  resources :users do

    member do
      get :edit_profile
      get :profile
      resources :profiles, only:[:update, :destroy]      
    end

  end
 
  resources :bvotes, only: [:create]
  resources :lists do
      member do
        put :upvote
        get :upvote
        put :downvote
        get :downvote
        put :approve
        put :feature
        end 
    resources :comments, except: [:index, :new, :show] do
      member do
        put :select        
        get :reply
        put :upvote
        get :upvote
        put :downvote
        get :downvote
      end
    end
  end

  resources :polls do
      member do
        put :upvote
        put :downvote
        put :approve
        put :feature
      end
    resources :comments, except: [:index, :new, :show] do
      member do
        put :select
        get :reply
        put :upvote        
        put :downvote
      end
    end
  end


  resources :photos
  resources :pictures
	root 'home#index'

   resources :questions do
    member do
    put :upvote
    put :downvote
    put :approve 
    put :feature   
    end      
    resources :comments, except: [:index, :new, :show] do
      member do
        put :select
        get :reply
        put :upvote        
        put :downvote
      end
    end
  end

  
  
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

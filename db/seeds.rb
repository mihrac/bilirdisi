# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Category.all.size < 1
categories = Category.create!([{ name: 'Cilt Bakımı'},
{ name: 'Aşk İlişkileri'},
{ name: 'Ağız Diş Bakımı'},
{ name: 'Alışveriş Hediyeler'},
{ name: 'Anne Bebek'},
{ name: 'Cinsel Yaşam'},
{ name: 'Diğer'},
{ name: 'Eğitim Kariyer'},
{ name: 'Gündem Ekonomi'},
{ name: 'Güzellik Bakım'},
{ name: 'İnternet Teknoloji'},
{ name: 'Kişilik Karakter'},
{ name: 'Bilirdişi Hakkında'},
{ name: 'Kültür Sanat'},
{ name: 'Magazin Filmler'},
{ name: 'Moda Stil'},
{ name: 'Nasılım?'},
{ name: 'Özel Günler Hijyen'},
{ name: 'Pürüzsüz Vücut Tüyler'},
{ name: 'Saç Bakım'},
{ name: 'Sağlık Diyet'},
{ name: 'Spor'},
{ name: 'Tatil Seyahat'},
{ name: 'Toplum Sosyal İlişkiler'},
{ name: 'Yemek Tarifler'}])
end

require 'csv'
# puts "Creating First User..."
# User.create!(
# 	username: "yonetici",
# 	email:"gorselsanat@gmail.com",
# 	password:"Bd98986666/*@",


# 	)
puts "Importing users..."
CSV.foreach(Rails.root.join("xf_user.csv"), headers: true, encoding: 'iso-8859-9:utf-8') do |row|
  User.create! do |user|  
    user.username = row[1]
    user.email = row[2]
    user.id = row[0]
    o = [('a'..'z'), (0..8),('A'..'Z')].map(&:to_a).flatten
    user.password = (0...15).map { o[rand(o.length)] }.join
    user.save!
 end
end
puts "Finished"
puts "...."
puts "Importing questions..."
CSV.foreach(Rails.root.join("xf_thread.csv"), headers: true, encoding: 'iso-8859-9:utf-8') do |row|
  Question.create! do |q|

    q.title = row[2]
    q.category_id = 7
    q.user_id = row[5]
    q.id = row[0]
    q.save!
 end
end
puts "Finished"
puts "...."

puts "Importing Question comments..."
CSV.foreach(Rails.root.join("xf_post.csv"), headers: true, quote_char: '|', encoding: 'iso-8859-9:utf-8') do |row|
  Comment.create! do |c|

    c.content = row[5].to_s
    c.commentable_id = row[1]
    c.commentable_type = "Question"
    c.user_id = row[2]
    c.id = row[0]
    c.save!
 end
end
puts "Finished"
puts "...."
puts "ordering comment"
questions = Question.all
questions.each do |qu|
	qu.body = qu.comments.order('created_at ASC').first.content if qu.comments.present?
	qu.save!
	qu.comments.first.destroy! if qu.comments.present?
	end

puts "Finished"
puts "..."
puts "Importing Blogs..."
CSV.foreach(Rails.root.join("bd_posts.csv"), headers: true, quote_char: '|', encoding: 'iso-8859-9:utf-8') do |row|
  Blog.create! do |b|

    b.title = row[5]
    b.body = row[4]
    b.category_id = 7
    b.user_id = row[1]
    b.id = row[0]
    b.save!
 end
end
puts "Finished"
puts "..."

ActiveRecord::Base.connection.tables.each { |t|     ActiveRecord::Base.connection.reset_pk_sequence!(t) }

puts "Importing Blog comments..."
CSV.foreach(Rails.root.join("bd_comments.csv"), headers: true, quote_char: '|', encoding: 'iso-8859-9:utf-8') do |row|
  Comment.create! do |co|

    co.content = row[8].to_s
    co.commentable_id = row[1]
    co.commentable_type = "Blog"
    co.user_id = row[14]
    
    
 end
end
puts "Finished: Importing Blog comments..."

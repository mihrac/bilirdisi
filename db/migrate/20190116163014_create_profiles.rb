class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.text :hakkimda
      t.integer :cinsiyet
      t.date :dogum_tarihi
      t.integer :medeni

      t.timestamps
    end
  end
end

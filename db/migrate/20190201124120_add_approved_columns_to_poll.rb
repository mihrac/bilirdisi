class AddApprovedColumnsToPoll < ActiveRecord::Migration[5.2]
  def change
    add_column :polls, :approved, :boolean, default: false
    add_column :lists, :approved, :boolean, default: false    
    change_column :questions, :approved, :boolean, default: false
    add_column :polls, :deleted, :boolean, default: false
    add_column :lists, :deleted, :boolean, default: false
    add_column :blogs, :deleted, :boolean, default: false
    add_column :questions, :deleted, :boolean, default: false
    add_column :comments, :deleted, :boolean, default: false
  end
end

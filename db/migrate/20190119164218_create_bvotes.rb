class CreateBvotes < ActiveRecord::Migration[5.2]
  def change
    create_table :bvotes do |t|
      t.references :user, foreign_key: true
      t.references :vote_option, foreign_key: true

      t.timestamps
    end
  end
end

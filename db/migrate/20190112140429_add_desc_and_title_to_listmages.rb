class AddDescAndTitleToListmages < ActiveRecord::Migration[5.2]
  def change
    add_column :listmages, :title, :string
    add_column :listmages, :description, :text
  end
end

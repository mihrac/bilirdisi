class AddDraftColumnToQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :draft, :boolean, default: true
  end
end

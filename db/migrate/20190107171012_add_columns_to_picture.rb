class AddColumnsToPicture < ActiveRecord::Migration[5.2]
  def change
    add_column :pictures, :ititle, :string
    add_column :pictures, :idescription, :text
  end
end

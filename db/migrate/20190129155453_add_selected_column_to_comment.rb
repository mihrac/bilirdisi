class AddSelectedColumnToComment < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :selected, :boolean, default: false
  end
end

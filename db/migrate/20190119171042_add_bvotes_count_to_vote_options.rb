class AddBvotesCountToVoteOptions < ActiveRecord::Migration[5.2]
  def self.up
    add_column :vote_options, :bvotes_count, :integer, null: false, default: 0
  end

  def self.down
    remove_column :vote_options, :bvotes_count
  end
end

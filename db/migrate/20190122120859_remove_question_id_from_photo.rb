class RemoveQuestionIdFromPhoto < ActiveRecord::Migration[5.2]
  def change
  	remove_column :photos, :question_id
  end
end

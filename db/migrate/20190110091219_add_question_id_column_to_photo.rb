class AddQuestionIdColumnToPhoto < ActiveRecord::Migration[5.2]
  def change
    add_column :photos, :question_id, :integer
  end
end

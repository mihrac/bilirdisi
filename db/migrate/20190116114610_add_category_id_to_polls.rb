class AddCategoryIdToPolls < ActiveRecord::Migration[5.2]
  def change
    add_reference :polls, :category, foreign_key: true
  end
end

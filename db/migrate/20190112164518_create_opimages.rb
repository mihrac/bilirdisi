class CreateOpimages < ActiveRecord::Migration[5.2]
  def change
    create_table :opimages do |t|
      t.string :image
      t.references :vote_option, foreign_key: true

      t.timestamps
    end
  end
end

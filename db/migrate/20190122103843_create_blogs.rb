class CreateBlogs < ActiveRecord::Migration[5.2]
  def change
    create_table :blogs do |t|
      t.string :title
      t.string :slug
      t.text :body
      t.boolean :published, default: false
      t.boolean :approved, default: false
      t.boolean :featured, default: false
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
    add_index :blogs, :slug, unique: true
  end
end



    
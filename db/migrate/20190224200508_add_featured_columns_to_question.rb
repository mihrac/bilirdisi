class AddFeaturedColumnsToQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :featured, :boolean, default: false    
    add_column :lists, :featured, :boolean, default: false
    add_column :polls, :featured, :boolean, default: false
  end
end

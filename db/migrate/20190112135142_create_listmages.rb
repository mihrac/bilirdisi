class CreateListmages < ActiveRecord::Migration[5.2]
  def change
    create_table :listmages do |t|
      t.references :list, foreign_key: true
      t.string :image

      t.timestamps
    end
  end
end

class AddLevelColumnToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :level, :integer, default: 0
    add_column :users, :points, :integer, default: 10
  end
end

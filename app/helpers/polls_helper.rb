module PollsHelper
  def visualize_bvotes_for(option)
    content_tag :div, class: 'progress' do
      content_tag :div, class: 'progress-bar',
                  style: "width: #{option.poll.normalized_bvotes_for(option)}%" do
        "#{option.bvotes.count}"
      end
    end
  end
end
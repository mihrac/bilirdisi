class ProfilesController < ApplicationController
  before_action :authenticate_user!, except:[:index, :show]
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :check_user, only: [:edit, :update, :destroy]
  impressionist actions: [:show,:index]
  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
  end

  # GET /profiles/new
  def new
    @profile = current_user.build_profile
    @profile.avatars.build
  end

  # GET /profiles/1/edit
  def edit
    @profile.avatars
  end

  # POST /profiles
  # POST /profiles.json
  def create

    @profile = current_user.build_profile(profile_params)

    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    @user = User.friendly.find(params[:id]) 
    if @user == current_user or current_user.admin?
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to profile_user_path, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @user.profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end

  end #admin ya da geçerli kullanıcı
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.require(:profile).permit(:hakkimda, :cinsiyet, :dogum_tarihi, :medeni, avatars_attributes:[:image, :_destroy])
    end


  def check_user  
    redirect_to root_path, alert: "buna yetkiniz yok üzgünüz." unless current_user == @profile.user or current_user.admin?
  end  





end

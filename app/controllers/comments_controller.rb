class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_commentable
  before_action :set_comment, only: [ :reply, :edit, :update, :destroy, :upvote, :downvote, :select ]
  before_action :check_commenter, only: [:edit, :update, :destroy]

  def reply
    @reply = @commentable.comments.build(parent: @comment)
  end

  def create
    @comment = @commentable.comments.new(comment_params)
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        @comment.user.increment!(:points, 1)
        format.html { redirect_to @commentable, notice: "Yorum başarıyla oluşturuldu."}
        format.json { render json: @comment }
        format.js
      else
        format.html { render :back, notice: "Yorum oluşturulamadı" }
        format.json { render json: @comment.errors }
        format.js
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @commentable, notice: "Yorum başarıyla güncellendi"}
        format.json { render json: @comment }
        format.js
      else
        format.html { render :back, notice: "Yorum güncellenemedi." }
        format.json { render json: @comment.errors }
        format.js
      end
    end
  end

  def destroy
    @comment.destroy if @comment.errors.empty?
    @comment.user.decrement!(:points, 1)  
    respond_to do |format|
      format.html { redirect_to @commentable, notice: "Yorum başarıyla silindi"}
      format.json { head :no_content }
      format.js
    end
  end

def select
  @selected_comment = @commentable.comments.where(selected: true).size
  if @selected_comment < 1   
    if @comment.parent_id.nil?
      @comment.update_attribute(:selected, true)
      @comment.user.increment!(:points, 5)  
      redirect_to @commentable, notice: "Tebrikler. Paylaşımınıza en faydalı cevabı buldunuz."
    end
  else
    redirect_to @commentable, alert: "Zaten bir cevabı seçmişsiniz."
  end
  
end




def upvote  #
  unless current_user == @comment.user
   unless (current_user.voted_up_on? @comment)
     @comment.upvote_by current_user
     @comment.user.increment!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
   end  

  def downvote 
      unless current_user == @comment.user
   unless current_user.voted_down_on? @comment
     @comment.downvote_by current_user
     @comment.user.decrement!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
  end








  private

  def set_commentable
    resource, id = request.path.split('/')[1,2]
    @commentable = resource.singularize.classify.constantize.friendly.find(id)
  end

  def set_comment
    begin
      @comment = @commentable.comments.find(params[:id])
    rescue => e
      logger.error "#{e.class.name} : #{e.message}"
      @comment = @commentable.comments.build
      @comment.errors.add(:base, :recordnotfound, message: "Böyle bir kayıt mevcut değil. Silinmiş olabilir.")
    end
  end

  def comment_params
    params.require(:comment).permit(:content, :parent_id)
  end


def check_commenter
 @comment.errors.add(:base, message: "Bu yorumu silme yetkiniz yok.") unless @comment.user == current_user or current_user.admin?
end

def decrement!(attribute, by = 1)
  decrement(attribute, by).update_attribute(attribute, self[attribute])
end

def increment!(attribute, by = 1)
  increment(attribute, by).update_attribute(attribute, self[attribute])
end

end
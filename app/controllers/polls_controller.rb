class PollsController < ApplicationController
  before_action :authenticate_user!, except:[:index, :show]
  before_action :set_poll, only: [:show, :edit, :update, :destroy, :upvote, :downvote, :approve, :feature]
  impressionist actions: [:show,:index]



  def index
    @polls = Poll.order('created_at DESC')
  end

  def show
    #@poll = Poll.includes(:vote_options).find_by_id(params[:id])
    @bvote = Bvote.new
  end

  def new
    @poll = Poll.new    
    @poll.vote_options.build
  end

     
  

  def edit
    
  end

  def update
   # @poll = Poll.find_by_id(params[:id])
    if @poll.update(poll_params)
      flash[:success] = 'Poll was updated!'
      redirect_to polls_path
    else
      render 'edit'
    end
  end

  def create
    @poll = current_user.polls.build(poll_params)
    if @poll.save
      @poll.user.increment!(:points, 3)
      flash[:success] = 'Anket Başarıyla oluşturuldu!'
      redirect_to poll_path(@poll)
    else
      render 'new'
    end
  end

  def destroy
    #@poll = Poll.find_by_id(params[:id])
    if @poll.destroy
      @poll.user.decrement!(:points, 3)
      flash[:success] = 'Anket silindi!'
    else
      flash[:warning] = 'İşlem hatalı. silinemiyor...'
    end
    redirect_to polls_path
  end



  def upvote  #
  unless current_user == @poll.user
   unless (current_user.voted_up_on? @poll)
     @poll.upvote_by current_user
     @poll.user.increment!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
   end  

  def downvote 
      unless current_user == @poll.user
   unless current_user.voted_down_on? @poll
     @poll.downvote_by current_user
     @poll.user.decrement!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
  end


def approve
  if @poll.approved?
    @poll.update_attribute(:approved, false)
  else
    @poll.update_attribute(:approved, true)
  end
  redirect_back(fallback_location: root_path)
end

def feature
  if @poll.featured?
    @poll.update_attribute(:featured, false)
  else
    @poll.update_attribute(:featured, true)
  end
  redirect_back(fallback_location: root_path)
end



  private


def set_poll
      @poll = Poll.friendly.find(params[:id])
    end



  def poll_params
    params.require(:poll).permit(:topic, :description, :category_id, vote_options_attributes: [:id, :title, :_destroy, opimages_attributes:[:image, :vote_option_id, :_destroy]])
  end


def decrement!(attribute, by = 1)
  decrement(attribute, by).update_attribute(attribute, self[attribute])
end

def increment!(attribute, by = 1)
  increment(attribute, by).update_attribute(attribute, self[attribute])
end


end
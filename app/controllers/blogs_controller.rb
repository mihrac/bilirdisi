class BlogsController < ApplicationController
   before_action :authenticate_user!, except:[:index, :show]
  before_action :set_blog, only: [:show, :edit, :update, :destroy, :upvote, :downvote, :approve, :feature]
  impressionist actions: [:show,:index]
  # GET /blogs
  # GET /blogs.json
  def index
    @blogs = Blog.where(approved: true).paginate(page: params[:page]).order('created_at DESC')
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
    
  end



  # GET /blogs/new
  def new
     @blog = Blog.new
     @blog.blog_images.build     
  

  end

  # GET /blogs/1/edit
  def edit
    @blog.blog_images.build 
  end

  # POST /blogs
  # POST /blogs.json
  def create
    @blog = current_user.blogs.build(blog_params)

    respond_to do |format|
      if @blog.save
        @blog.user.increment!(:points, 5) 
        format.html { redirect_to @blog, notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
        
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end



  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to @blog, notice: 'Blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @blog.destroy
    @blog.user.decrement!(:points, 5) 
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


def upvote  #
  unless current_user == @blog.user
   unless (current_user.voted_up_on? @blog)
     @blog.upvote_by current_user
     @blog.user.increment!(:points, 1) 
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
   end  

  def downvote 
      unless current_user == @blog.user
   unless current_user.voted_down_on? @blog
     @blog.downvote_by current_user
     @blog.user.decrement!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
  end

def approve
  if @blog.approved?
    @blog.update_attribute(:approved, false)
  else
    @blog.update_attribute(:approved, true)
  end
  redirect_back(fallback_location: root_path)
  
end

def feature
  if @blog.featured?
    @blog.update_attribute(:featured, false)
  else
    @blog.update_attribute(:featured, true)
  end
  redirect_back(fallback_location: root_path)
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      @blog = Blog.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      params.require(:blog).permit(:title, :body, :published, :approved, :featured, :user_id, :category_id, blog_images_attributes:[:image, :_destroy])
    end
end



def decrement!(attribute, by = 1)
  decrement(attribute, by).update_attribute(attribute, self[attribute])
end

def increment!(attribute, by = 1)
  increment(attribute, by).update_attribute(attribute, self[attribute])
end
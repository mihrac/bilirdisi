class ListsController < ApplicationController
  before_action :authenticate_user!, except:[:index, :show]
  before_action :set_list, only: [:show, :edit, :update, :destroy, :upvote, :downvote, :approve, :feature]
  impressionist actions: [:show,:index]

  # GET /lists
  # GET /lists.json
  def index
    @lists = List.all
  end

  # GET /lists/1
  # GET /lists/1.json
  def show
    @listmages = @list.listmages.order('created_at ASC')
  end

  # GET /lists/new
  def new
    @list = List.new
  end

  # GET /lists/1/edit
  def edit
    @list.listmages.order('created_at ASC')
  end

  # POST /lists
  # POST /lists.json
  def create
    @list = current_user.lists.build(list_params)

    respond_to do |format|
      if @list.save
        @list.user.increment!(:points, 5)
        format.html { redirect_to @list, notice: 'List was successfully created.' }
        format.json { render :show, status: :created, location: @list }
      else
        format.html { render :new }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lists/1
  # PATCH/PUT /lists/1.json
  def update
    respond_to do |format|
      if @list.update(list_params)
        format.html { redirect_to @list, notice: 'List was successfully updated.' }
        format.json { render :show, status: :ok, location: @list }
      else
        format.html { render :edit }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lists/1
  # DELETE /lists/1.json
  def destroy
    @list.destroy
    respond_to do |format|
      @list.user.decrement!(:points, 5)
      format.html { redirect_to lists_url, notice: 'List was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


def upvote  #
  unless current_user == @list.user
   unless (current_user.voted_up_on? @list)
     @list.upvote_by current_user
     @list.user.increment!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
   end  

  def downvote 
      unless current_user == @list.user
   unless current_user.voted_down_on? @list
     @list.downvote_by current_user
     @list.user.decrement!(:points, 1)  
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
  end


def approve
  if @list.approved?
    @list.update_attribute(:approved, false)
  else
    @list.update_attribute(:approved, true)
  end
  redirect_back(fallback_location: root_path)
end

def feature
  if @list.featured?
    @list.update_attribute(:featured, false)
  else
    @list.update_attribute(:featured, true)
  end
  redirect_back(fallback_location: root_path)
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      @list = List.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def list_params
      params.require(:list).permit(:title, :description, :user_id, :category_id, listmages_attributes:[:image, :title, :description, :_destroy])
    end



    def decrement!(attribute, by = 1)
  decrement(attribute, by).update_attribute(attribute, self[attribute])
end

def increment!(attribute, by = 1)
  increment(attribute, by).update_attribute(attribute, self[attribute])
end

    
end

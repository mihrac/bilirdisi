class UsersController < ApplicationController
  before_action :authenticate_user!, except:[:index, :show]
before_action :set_user, only: [:show, :edit_profile, :profile] 
before_action :check_user, only: [:edit_profile, :edit, :update, :destroy]
impressionist actions: [:show,:index]


def index
	@users = User.paginate(page: params[:page]).order('created_at DESC')
	
end

  def show
  	@user = User.friendly.find(params[:id])
  	@profile = @user.profile
  	
  end

def edit_profile
	@profile = @user.profile
	@profile.avatars.build
end

def profile
	@profile = @user.profile
	@avatars = @profile.avatars
	
end



private

def set_user

@user = User.friendly.find(params[:id])	
end

def check_user
  unless current_user == @user or current_user.admin?
    redirect_to users_path, alert: "buna yetkiniz yok üzgünüz."
  end
  
end

end

class BvotesController < ApplicationController
  before_action :authenticate_user!, only:[:create]
  def create
    if current_user && params[:poll] && params[:poll][:id] && params[:vote_option] && params[:vote_option][:id]
      @poll = Poll.find_by_id(params[:poll][:id])
      @option = @poll.vote_options.find_by_id(params[:vote_option][:id])
      if @option && @poll && !current_user.bvoted_for?(@poll)
        current_user.bvotes.create({vote_option_id: @option.id})
        current_user..decrement!(:points, 1)
         @poll.user.decrement!(:points, 1) unless current_user == @poll.user
        #redirect_to @poll ben ekledim
      else
        render js: 'alert(\'Your vote cannot be saved. Have you already voted?\');'
      end
    else
      render js: 'alert(\'Your vote cannot be saved.\');'

    end
  end
end


private

def decrement!(attribute, by = 1)
  decrement(attribute, by).update_attribute(attribute, self[attribute])
end

def increment!(attribute, by = 1)
  increment(attribute, by).update_attribute(attribute, self[attribute])
end
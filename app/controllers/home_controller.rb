class HomeController < ApplicationController
  impressionist actions: [:index]
  def index

  	@polls = Poll.where(featured: true).order('created_at DESC').limit(5)
  	@questions = Question.where(featured: true).order('created_at DESC').limit(4)
  	@lists = List.where(featured: true).order('created_at DESC').limit(4)
  	@blogs = Blog.where(featured: true).order('created_at DESC').limit(5)
  	@question = @questions.first
  	@blog = @blogs.first
  	@list = @lists.first
  	@poll = @polls.first
  end
end

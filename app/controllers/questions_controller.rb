class QuestionsController < ApplicationController
  before_action :authenticate_user!, except:[:index, :show]
  before_action :set_question, only: [:show, :edit, :update, :destroy, :upvote, :downvote, :approve, :feature]
  before_action :check_user, only: [:edit, :update, :destroy]
  impressionist actions: [:show,:index]

  # GET /questions
  # GET /questions.json
  def index
    
    @questions = Question.where(approved: true).paginate(page: params[:page]).order('created_at DESC')

  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @pictures = @question.pictures.order('created_at ASC')

  
  end

  # GET /questions/new
  def new
    @question = Question.new
    @question.pictures.build
  end

  # GET /questions/1/edit
  def edit
    @question.pictures.order('created_at ASC')
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = current_user.questions.build(question_params)
    

    respond_to do |format|
      if @question.save
        @question.user.increment!(:points, 3)
        format.html { redirect_to @question, notice: 'Soru başarılı bir şekilde oluşturuldu' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @question, notice: 'Soru başarılı bir şekilde güncellendi' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    @question.user.decrement!(:points, 3)
    respond_to do |format|
      format.html { redirect_to questions_url, notice: 'Soru başarılı bir şekilde silindi' }
      format.json { head :no_content }

    end
  end


def upvote  #
  unless current_user == @question.user
   unless (current_user.voted_up_on? @question)
     @question.upvote_by current_user
     @question.user.increment!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
   end  

  def downvote   
      unless current_user == @question.user
   unless current_user.voted_down_on? @question
     @question.downvote_by current_user
     @question.user.decrement!(:points, 1)   
    respond_to do |format|
    format.html { redirect_back(fallback_location: root_path) }
    format.js { render layout: false }
      end    
     end
   end
  end


def approve
  if @question.approved?
    @question.update_attribute(:approved, false)
  else
    @question.update_attribute(:approved, true)
  end
  redirect_back(fallback_location: root_path)
end

def feature
  if @question.featured?
    @question.update_attribute(:featured, false)
  else
    @question.update_attribute(:featured, true)
  end
  redirect_back(fallback_location: root_path)
end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title, :body, :draft, :approved, :category_id, pictures_attributes:[:id, :image, :question_id, :ititle, :idescription, :_destroy])
    end

def check_user  
    redirect_to root_path, alert: "Buna yetkiniz yok üzgünüz." unless current_user == @question.user or current_user.admin?
  end  


def decrement!(attribute, by = 1)
  decrement(attribute, by).update_attribute(attribute, self[attribute])
end

def increment!(attribute, by = 1)
  increment(attribute, by).update_attribute(attribute, self[attribute])
end
  





end

class Poll < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :vote_options, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  accepts_nested_attributes_for :vote_options, :reject_if => :all_blank, :allow_destroy => true
  validates :topic, presence: true
  extend FriendlyId
  friendly_id :topic, use: :slugged
  acts_as_votable
  is_impressionable

  def normalized_bvotes_for(option)
    bvotes_summary == 0 ? 0 : (option.bvotes.count.to_f / bvotes_summary) * 100
  end

  def bvotes_summary
    vote_options.inject(0) {|summary, option| summary + option.bvotes.count}
end


end

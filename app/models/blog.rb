class Blog < ApplicationRecord
	has_many :blog_images, dependent: :destroy
	accepts_nested_attributes_for :blog_images, allow_destroy: true
	has_many :photos, dependent: :destroy
	accepts_nested_attributes_for :photos, allow_destroy: true
  belongs_to :user
  belongs_to :category
  extend FriendlyId
  friendly_id :title, use: [:finders, :slugged]
  acts_as_votable
  has_many :comments, as: :commentable, dependent: :destroy
  is_impressionable
end

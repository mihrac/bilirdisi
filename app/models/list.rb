class List < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :listmages, dependent: :destroy, inverse_of: :list
  accepts_nested_attributes_for :listmages, allow_destroy: true
  has_many :comments, as: :commentable, dependent: :destroy
  extend FriendlyId
  friendly_id :title, use: :slugged
  acts_as_votable
  is_impressionable
end

class Listmage < ApplicationRecord
  belongs_to :list
  validates :image, presence: true, on: :create
  mount_uploader :image, ListimageUploader
end

class VoteOption < ApplicationRecord
 belongs_to :poll
  has_many :bvotes, dependent: :destroy
  has_many :users, through: :bvotes
  has_many :opimages, dependent: :destroy
  accepts_nested_attributes_for :opimages, allow_destroy: true

validates :title, presence: true
end

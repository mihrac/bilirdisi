class BlogImage < ApplicationRecord
  belongs_to :blog
  validates :image, presence: true
  mount_uploader :image, BlogImageUploader
end

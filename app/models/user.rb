class User < ApplicationRecord
  include ActiveModel::Validations
  validates :password, password_strength: {min_entropy: 18}
  enum unvan: ["Bilişim uzmanı", "Moderator", "Göğüs hastalıkları uzmanı", "Admin", "Güzellik uzmanı", "Psikolog", "Avukat", "Yaşam koçu", "Hekim", "Eczacı", "Yazılımcı"]
	has_many :questions, dependent: :destroy
  has_many :blogs, dependent: :destroy
  has_one :profile, dependent: :destroy
  accepts_nested_attributes_for :profile, allow_destroy: true 
	has_many :comments, dependent: :destroy
	has_many :lists, dependent: :destroy  
	has_many :polls, dependent: :destroy
  has_many :bvotes, dependent: :destroy
  #has_many :avatars, dependent: :destroy
  #accepts_nested_attributes_for :avatars, allow_destroy: true
	#has_many :vote_options, through: :bvotes
  validates :username, presence: true
  validates :username, uniqueness: true
  after_create :create_profile, on: :create
  
  extend FriendlyId
    friendly_id :username, use: :slugged
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :lockable, :trackable, :timeoutable#, :encryptable
is_impressionable
acts_as_voter





  def bvoted_for?(poll)
    bvotes.any? {|v| v.vote_option.poll == poll}
end


  def create_profile
    profile = self.create_profile!  
     self.increment!(:points, 5)  
  end




end

class Category < ApplicationRecord
	has_many :questions
	has_many :blogs
	has_many :lists
	has_many :polls
	#has_many :subcategories, :class_name => "Category", :foreign_key => "parent_id", :dependent => :destroy
  	#belongs_to :parent_category, :class_name => "Category"
end

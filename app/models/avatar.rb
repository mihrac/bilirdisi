class Avatar < ApplicationRecord
  belongs_to :profile
  
  mount_uploader :image, AvatarUploader
end

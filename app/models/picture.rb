class Picture < ApplicationRecord
  belongs_to :question
  validates :image, presence: true, on: :create
  mount_uploader :image, PictureUploader

end
class Question < ApplicationRecord  
	  belongs_to :user
  	belongs_to :category
  	has_many :comments, as: :commentable, dependent: :destroy    
  	has_many :pictures, dependent: :destroy, inverse_of: :question
  	accepts_nested_attributes_for :pictures, allow_destroy: true
  	extend FriendlyId
  	friendly_id :title, use: [:finders, :slugged]
    acts_as_votable
    is_impressionable



self.per_page = 10


end

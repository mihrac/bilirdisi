class Opimage < ApplicationRecord
  belongs_to :vote_option
  mount_uploader :image, OpimageUploader
end

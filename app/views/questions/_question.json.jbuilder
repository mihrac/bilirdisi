json.extract! question, :id, :title, :body, :approved, :category_id, :pictures, :created_at, :updated_at
json.url question_url(question, format: :json)

json.extract! blog, :id, :title, :body, :published, :approved, :featured, :user_id, :category_id, :created_at, :updated_at
json.url blog_url(blog, format: :json)

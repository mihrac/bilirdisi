json.extract! profile, :id, :user_id, :hakkimda, :cinsiyet, :dogum_tarihi, :medeni, :created_at, :updated_at
json.url profile_url(profile, format: :json)

json.extract! list, :id, :title, :description, :user_id, :category_id, :created_at, :updated_at
json.url list_url(list, format: :json)
